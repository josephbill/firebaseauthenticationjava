package com.emobilis.testfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.emobilis.testfirebase.adapter.ProductAdapter;
import com.emobilis.testfirebase.model.ProductDescriptionModel;
import com.emobilis.testfirebase.model.ProductNameModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ProductsActivity extends AppCompatActivity {
    //view objects
    EditText editTextName;
    EditText editDesc;
    Button buttonAddProduct;
    ListView listViewProducts;

    //a list to store all the artist from firebase database
    List<ProductNameModel> productNameModelList;

    //our database reference object
    DatabaseReference databaseProducts;

    //decaring my sweetalert
    SweetAlertDialog errorDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        //intialize
        databaseProducts = FirebaseDatabase.getInstance().getReference("products");
        //ref to my views
        editTextName = findViewById(R.id.editTextName);
        editDesc = findViewById(R.id.editTextDesc);
        buttonAddProduct = findViewById(R.id.buttonAddProduct);
        listViewProducts = findViewById(R.id.listViewProducts);

        //list to store my items
        productNameModelList = new ArrayList<>();
        //onclick for add
        buttonAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    addProduct();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        //set on click listener for our listview items
        listViewProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //getting the selected artist
                ProductNameModel productNameModel = productNameModelList.get(position);

                //creating an intent
                Intent intent = new Intent(getApplicationContext(), ProductDetails.class);

                //putting artist name and id to intent
                intent.putExtra("id", productNameModel.getProductId());
                intent.putExtra("name", productNameModel.getNameProduct());

                Log.d("ProductActivity" ,"Id " + productNameModel.getProductId() + "name " + productNameModel.getNameProduct());

                //starting the activity with intent
                startActivity(intent);
            }
        });

        //item to update or delete
        listViewProducts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ProductNameModel productNameModel = productNameModelList.get(position);
                showUpdateDeleteDialog(productNameModel.getProductId(), productNameModel.getNameProduct());
                return true;
            }
        });
    }

    private void addProduct() {
        //the first we do
        //pick inputs
        String name,desc;
        name = editTextName.getText().toString().trim();
        desc = editDesc.getText().toString().trim();

        //check entries
        if (TextUtils.isEmpty(name) && TextUtils.isEmpty(desc)){
            errorDialog  = new SweetAlertDialog(this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Enter name and description");
            errorDialog.show();
        } else {
            //loading dialog
            SweetAlertDialog sweetAlertDialogProgress = new SweetAlertDialog(this,SweetAlertDialog.PROGRESS_TYPE);
            sweetAlertDialogProgress.setTitle("uploading product");
            sweetAlertDialogProgress.show();
            //here my firebase write coding
            //first create an id for the records
            String id = databaseProducts.push().getKey();
            //new instance of our ProductNameModel
            ProductNameModel productNameModel = new ProductNameModel(id,name,desc);
            //use set value method to push my content to firebase
            databaseProducts.child(id).setValue(productNameModel);
            //dismiss the progress dialog
            sweetAlertDialogProgress.dismiss();
            //reset the input blocks
            editTextName.setText("");
            editDesc.setText("");
            //finally a notification message
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this,SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialog.setTitle("Product Added");
            sweetAlertDialog.setContentText("Product has been added to firebase real time database");
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    Toast.makeText(ProductsActivity.this, "Okay alert works", Toast.LENGTH_SHORT).show();
                }
            });
            sweetAlertDialog.show();
        }
    }

    //we will read our values in the lifecylce method
    //here we will use the value event listener to fetch our details
    @Override
    protected void onStart() {
        super.onStart();

        databaseProducts.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //read
                //clear our list
                productNameModelList.clear();
                //iterating over our nodes
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    //getting product
                    ProductNameModel productNameModel = snapshot.getValue(ProductNameModel.class);
                    //add this fetched records to my list
                    productNameModelList.add(productNameModel);
                }

                //creating adapter
                ProductAdapter adapter = new ProductAdapter(ProductsActivity.this, productNameModelList);
                //attaching adapter to the listview
                listViewProducts.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //error
                errorDialog.setTitle("an error occured");
                errorDialog.setTitle("an error occured");
                errorDialog.show();
            }
        });

    }
    //method to update a product
    private boolean updateProduct(String id, String name, String description) {
        //getting the specified product reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("products").child(id);

        //updating product Name model
        ProductNameModel productNameModel = new ProductNameModel(id, name, description);
        dR.setValue(productNameModel);
        Toast.makeText(getApplicationContext(), "Product Updated", Toast.LENGTH_LONG).show();
        return true;
    }

    //method to delete a product
    private boolean deleteProduct(String id) {
        //getting the specified product reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference("products").child(id);

        //removing product
        dR.removeValue();

        //getting the comments reference for the specified artist
        DatabaseReference drTracks = FirebaseDatabase.getInstance().getReference("ProductComments").child(id);

        //removing all tracks
        drTracks.removeValue();
        Toast.makeText(getApplicationContext(), "Product Deleted", Toast.LENGTH_LONG).show();

        return true;
    }

    //launch of the dialog (update product)
    private void showUpdateDeleteDialog(final String productId, String productName) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.update_product, null);
        dialogBuilder.setView(dialogView);

        final EditText editTextName = (EditText) dialogView.findViewById(R.id.editTextName);
        final EditText desc = (EditText) dialogView.findViewById(R.id.editDesc);
        final Button buttonUpdate = (Button) dialogView.findViewById(R.id.buttonUpdateProduct);
        final Button buttonDelete = (Button) dialogView.findViewById(R.id.buttonDeleteProduct);

        dialogBuilder.setTitle(productName);
        final AlertDialog b = dialogBuilder.create();
        b.show();
//       end of the layout popup

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString().trim();
                String descs = desc.getText().toString().trim();
                if (!TextUtils.isEmpty(name)) {
                    updateProduct(productId, name, descs);
                    b.dismiss();
                } else {
                    Toast.makeText(ProductsActivity.this, "I need a new product name", Toast.LENGTH_SHORT).show();
                }
            }
        });


        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProduct(productId);
                b.dismiss();
            }
        });
    }

}
