package com.emobilis.testfirebase.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.emobilis.testfirebase.R;
import com.emobilis.testfirebase.model.ProductDescriptionModel;

import java.util.List;

public class ProductCommentsAdapter extends ArrayAdapter<ProductDescriptionModel> {
    private Activity context;
    private List<ProductDescriptionModel> productDescriptionModels;

    public ProductCommentsAdapter(Activity context, List<ProductDescriptionModel> productDescriptionModels) {
        super(context, R.layout.layout_product, productDescriptionModels);
        this.context = context;
        this.productDescriptionModels = productDescriptionModels;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_product, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.name);
        TextView textViewRating = (TextView) listViewItem.findViewById(R.id.description);

        ProductDescriptionModel productDescriptionModel = productDescriptionModels.get(position);
        textViewName.setText(productDescriptionModel.getDescName());
        textViewRating.setText(String.valueOf(productDescriptionModel.getDescRating()));

        return listViewItem;
    }
}
