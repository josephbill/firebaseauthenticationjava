package com.emobilis.testfirebase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.emobilis.testfirebase.R;
import com.emobilis.testfirebase.model.Upload;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DisplayAdapter extends RecyclerView.Adapter<DisplayAdapter.DisplayViewHolder> {
    //declare
    private Context context;  //attach the adapter to activity
    private List<Upload> uploadList; //ref to our list

    //constructor : that shall pick the values from the activity
    public DisplayAdapter(Context mCtx,List<Upload> uploads){
        context = mCtx;
        uploadList = uploads;

    }

    //create the view holder class
    //ref the views inside the recycled item
    public static class DisplayViewHolder extends RecyclerView.ViewHolder{
        TextView imageName;
        ImageView mimage;

        public DisplayViewHolder(@NonNull View itemView) {
            super(itemView);
            //ref the views
            imageName = itemView.findViewById(R.id.textImage);
            mimage = itemView.findViewById(R.id.image);
        }
    }

   //inflate the view
    @NonNull
    @Override
    public DisplayAdapter.DisplayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.display_item, parent, false);
        return new DisplayViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DisplayAdapter.DisplayViewHolder holder, int position) {
        Upload itemPosition = uploadList.get(position);
        //set text name for image
        holder.imageName.setText(itemPosition.getName());
        //use picasso to load image
//        Picasso.get().load(itemPosition.getImageURL()).fit().centerCrop().into(holder.mimage);
        Glide.with(context).load(itemPosition.getImageURL()).into(holder.mimage);

    }

    //
    @Override
    public int getItemCount() {
        return uploadList.size();
    }
}
