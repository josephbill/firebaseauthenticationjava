package com.emobilis.testfirebase.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.emobilis.testfirebase.R;
import com.emobilis.testfirebase.model.ProductNameModel;

import java.util.List;

public class ProductAdapter extends ArrayAdapter<ProductNameModel> {
    //first we create a context
    private Activity context;
    List<ProductNameModel> productNameModelList;

    //constructor
    public ProductAdapter(Activity context, List<ProductNameModel> productNameModels) {
        super(context, R.layout.layout_product, productNameModels);
        this.context = context;
        this.productNameModelList = productNameModels;
    }

    //ref our view objects and set data to them


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //layout inflater to inflate our layout
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.layout_product, null, true);
        //ref to the views in layout_product
        TextView textViewName = (TextView) listViewItem.findViewById(R.id.name);
        TextView textViewGenre = (TextView) listViewItem.findViewById(R.id.description);
        //create a ref to our data model class : to get the values
        ProductNameModel productNameModel = productNameModelList.get(position);
        textViewName.setText(productNameModel.getNameProduct());
        textViewGenre.setText(productNameModel.getDescProduct());
        //return the list
        return listViewItem;
    }
}
