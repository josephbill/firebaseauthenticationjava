package com.emobilis.testfirebase.model;

public class ProductDescriptionModel {
    //define the variables
    private String descId;
    private String descName;
    private String descRating;

    //empty constructor
    public ProductDescriptionModel(){

    }

    //populate the constructor
    public ProductDescriptionModel(String descId,String descName,String descRating){
        this.descId = descId;
        this.descName = descName;
        this.descRating = descRating;
    }

    //get and set

    public String getDescId() {
        return descId;
    }

    public void setDescId(String descId) {
        this.descId = descId;
    }

    public String getDescName() {
        return descName;
    }

    public void setDescName(String descName) {
        this.descName = descName;
    }

    public String getDescRating() {
        return descRating;
    }

    public void setDescRating(String descRating) {
        this.descRating = descRating;
    }
}
