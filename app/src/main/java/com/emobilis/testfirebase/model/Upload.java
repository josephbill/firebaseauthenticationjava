package com.emobilis.testfirebase.model;

public class Upload {
    //declare variables
    private String name;
    private String imageURL;


    public Upload(){
        //empty constructor
    }

    public Upload(String name_of_image,String image_URL){
        //check if name is null
        if (name_of_image.trim().equals("")) {
            name_of_image = "No Image Name attached";
        }

        name = name_of_image;
        imageURL = image_URL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
