package com.emobilis.testfirebase.model;

public class ProductNameModel {
    //define variables
    private String productId;
    private String nameProduct;
    private String descProduct;

    //define empty constructor
    public ProductNameModel(){

    }

    //now populate the constructor
    public ProductNameModel(String id,String nameProduct,String descProduct){
         this.productId = id;
         this.nameProduct = nameProduct;
         this.descProduct = descProduct;
    }

    //now set the get and set

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getDescProduct() {
        return descProduct;
    }

    public void setDescProduct(String descProduct) {
        this.descProduct = descProduct;
    }
}
