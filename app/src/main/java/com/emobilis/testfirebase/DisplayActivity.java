package com.emobilis.testfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.emobilis.testfirebase.adapter.DisplayAdapter;
import com.emobilis.testfirebase.model.Upload;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class DisplayActivity extends AppCompatActivity {
     //declare the recyclerview
     private RecyclerView mRecyclerView;
    private ProgressBar mProgressCircle;
     //declare my adapter
    DisplayAdapter adapter;
    //declare our firebase database ref
    private DatabaseReference mDatabaseRef;
    //declare your list class
    private List<Upload> mUploads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        //intialize firebase realtime db
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads"); //path from firebase
        //create a new instance of an  arraylist
        mUploads = new ArrayList<>();

        //intializing the views
        mRecyclerView = findViewById(R.id.recyclerDisplay);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));  //vertical
        //progress bar
        mProgressCircle = findViewById(R.id.progress_circle);

       //read data from firebase
       mDatabaseRef.addValueEventListener(new ValueEventListener() {
           @Override
           //read and display to the recyclerview
           //DataSnapshot contains the records in your firebase
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                   //for loop : iterate datasnapshot
               for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                      //create new object of the model class
                   Upload upload = snapshot.getValue(Upload.class);
                   //add the records to arraylist
                   mUploads.add(upload);
               }
               //add the data to the recyclerview
               //new object for the adapter
               adapter = new DisplayAdapter(DisplayActivity.this,mUploads);
               //now set the adapter instance to the recyclerview
               mRecyclerView.setAdapter(adapter);
               mProgressCircle.setVisibility(View.INVISIBLE);
           }

           @Override
           //terminates
           //DatabaseError : contains is the errors associated with a failure while reading the DB
           public void onCancelled(@NonNull DatabaseError databaseError) {
               Toast.makeText(DisplayActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
               mProgressCircle.setVisibility(View.INVISIBLE);
           }
       });

    }
}
