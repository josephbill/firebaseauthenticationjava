package com.emobilis.testfirebase;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class SharedPrefActivity extends AppCompatActivity {
    //declare our objects
    private TextView textView;
    private EditText editText;
    private Button applyTextButton;
    private Button saveButton;
    private Switch switch1;
    //define variable to ref our SHARED PREFERENCE OBJECt
    public static final String SHARED_PREFS = "sharedPrefs";
    //variables to set text and switch state
    public static final String TEXT = "text";
    public static final String SWITCH1 = "switch1";
    //variables to hold input from user
    private String text;
    private boolean switchOnOff;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_pref);

        //ref to views
        textView = (TextView) findViewById(R.id.textview);
        editText = (EditText) findViewById(R.id.edittext);
        applyTextButton = (Button) findViewById(R.id.apply_text_button);
        saveButton = (Button) findViewById(R.id.save_button);
        switch1 = (Switch) findViewById(R.id.switch1);

        //button onclick implementations
        applyTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get input from user
                text = editText.getText().toString().trim();
                textView.setText(text);
            }
        });

        //save data to shared pref
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });

        loadData();
        updateViews();

    }

    private void loadData() {
        //intialize the shared preferences class
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        //access the values/data using the key and sharedPreferences ref
        text = sharedPreferences.getString(TEXT,"");
        switchOnOff = sharedPreferences.getBoolean(SWITCH1,false);
    }


    //save data to shared pref
    private void saveData() {
        //intialize the shared preferences class
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        //create a new editor instance
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //store data in key value pairs
        //pick the set text
        String textsaved = textView.getText().toString().trim();
        editor.putString(TEXT,textsaved);
        //pick switch state
        Boolean switchState = switch1.isChecked();
        editor.putBoolean(SWITCH1,switchState);

        //apply the values to shared pref
        editor.apply();

        //toast
        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();

    }

    private void updateViews(){
          //set new entries from user
        textView.setText(text);
        switch1.setChecked(switchOnOff);
    }
}
