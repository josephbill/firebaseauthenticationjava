package com.emobilis.testfirebase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.emobilis.testfirebase.adapter.ProductCommentsAdapter;
import com.emobilis.testfirebase.model.ProductDescriptionModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ProductDetails extends AppCompatActivity {
    String name,id;
    DatabaseReference databaseComments;
    List<ProductDescriptionModel> productDescriptionModelList;
    TextView tvProduct,textViewRating;
    Button addProduct;
    EditText comment;
    ListView listComments;
    SeekBar seekBarRating;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

         tvProduct = findViewById(R.id.textViewProduct);
         comment = findViewById(R.id.edittextComment);
         addProduct = findViewById(R.id.buttonAddProduct);
         listComments = findViewById(R.id.listViewProducts);
         seekBarRating = findViewById(R.id.seekBarRating);
         textViewRating = findViewById(R.id.textViewRating);

        //get intents
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        id = intent.getStringExtra("id");

        tvProduct.setText(name);
        //attach this comment to the product id
        databaseComments = FirebaseDatabase.getInstance().getReference("ProductComments").child(id);

        //list to store the comments
        productDescriptionModelList = new ArrayList<>();

        seekBarRating.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textViewRating.setText(String.valueOf(progress));
                //here u've gotten the rating from user
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        addProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveRating();
            }
        });

    }

    private void saveRating() {
        //first pick comment and rating from user
        String comments = comment.getText().toString().trim();
        int rating = seekBarRating.getProgress();

        String ratings = String.valueOf(rating);

        if (!TextUtils.isEmpty(comments)) {
            String id  = databaseComments.push().getKey();  //create id for the record in firevase
            ProductDescriptionModel productDescriptionModel = new ProductDescriptionModel(id, comments, ratings);
            databaseComments.child(id).setValue(productDescriptionModel);
            Toast.makeText(this, "Comments Saved", Toast.LENGTH_LONG).show();
            comment.setText("");
        } else {
            Toast.makeText(this, "Please enter a comment first", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        databaseComments.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                productDescriptionModelList.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    ProductDescriptionModel productDescriptionModel = postSnapshot.getValue(ProductDescriptionModel.class);
                    productDescriptionModelList.add(productDescriptionModel);
                }
                ProductCommentsAdapter adapter = new ProductCommentsAdapter(ProductDetails.this, productDescriptionModelList);
                listComments.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(ProductDetails.this, "error occurred, try again later", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
